<!--
SPDX-FileCopyrightText: 2021, 2024 thewildmage <magic@thewildmage.dev>

SPDX-License-Identifier: MIT
-->

# dotfiles

My running default dotfiles and configuration, to make things more easily grab and go for myself.

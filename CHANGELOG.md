<!--
SPDX-FileCopyrightText: 2024 thewildmage <magic@thewildmage.dev>

SPDX-License-Identifier: MIT
-->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## v4.0.0

### Added

-   Make [REUSE](https://reuse.software) compliant (#30)
-   [Keep a Changelog](https://keepachangelog.com/en/1.1.0/) (#33)

### Changed

-   Update versions of pre-commit hooks (#31)

### Removed

-   Super-Linter (#30)
-   Dependabot (#31)

---

## Diffs

-   [v4.0.0...unreleased](https://git.suderman.dev/thewildmage/dotfiles/compare/v4.0.0...HEAD)
-   [3.3.0...4.0.0](https://git.suderman.dev/thewildmage/dotfiles/compare/v4.0.0...v3.3.0)
-   [3.2.0...3.3.0](https://git.suderman.dev/thewildmage/dotfiles/compare/v3.2.0...v3.3.0)
